package ru.inordic.alexeybaluev;

import java.util.Random;

public class CreateRandomArr {


    public final static int[] createRandomArr() {
        Random random = new Random();
        int rand = random.nextInt(10) + 20; //длина массива
        int[] arrCreateRandom = new int[rand];
        int number;
        for (int i = 0; i < rand; i++) {
            number = random.nextInt(200) - 100;// заполнить массив
            arrCreateRandom[i] = number;
        }

        return arrCreateRandom;
    }
}