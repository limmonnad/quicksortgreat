package ru.inordic.alexeybaluev;

public class QuickSortArray extends AbstractSorter {

    @Override
    protected int[] internalSort(int[] copy) {

        QuickSortArray.quickSort(copy,0,copy.length-1);

        return copy;
    }


    public static void quickSort(int[] arr, int from, int to) {
        if (from < to) {
            int divideIndex = partition(arr, from, to);
            quickSort(arr, from, divideIndex - 1);
            quickSort(arr, divideIndex, to);
        }
    }

    static int partition(int[] arr, int from, int to) {
        int rightIndex = to;
        int leftIndex = from;

        int pivot = arr[from + (to - from) / 2];
        while (leftIndex <= rightIndex) {

            while (arr[leftIndex] < pivot) {
                leftIndex++;
            }

            while (arr[rightIndex] > pivot) {
                rightIndex--;
            }

            if (leftIndex <= rightIndex) {
                int tmp = arr[rightIndex];
                arr[rightIndex] = arr[leftIndex];
                arr[leftIndex] = tmp;
                leftIndex++;
                rightIndex--;
            }
        }
        return leftIndex;
    }



}