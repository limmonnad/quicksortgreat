package RaznostVSlovah;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SlovaTest {

    @Test
    void removeZnaki() {

        Slova slova = new Slova();
        String s = "!!!привет";
        String s1 = slova.removeZnaki(s);

        Assertions.assertEquals("   привет", s1);


    }
}