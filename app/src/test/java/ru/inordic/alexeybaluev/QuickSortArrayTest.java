package ru.inordic.alexeybaluev;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QuickSortArrayTest {

    @Test
    void internalSort() {

        QuickSortArray quickSortArray = new QuickSortArray();
        int[] mas = {18, 10, 2};
        int[] arr = quickSortArray.internalSort(mas);
//        Assertions.assertArrayEquals(new int[]{18, 10, 2}, arr);
        Assertions.assertArrayEquals(new int[]{2, 10, 18}, arr);



    }
}
